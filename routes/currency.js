const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const { ensureAuthenticated } = require("../helpers/auth");

// load helper

// load schema
require("../models/Currency");
const Currency = mongoose.model("currency");

router.get("/api", (req, res) => {
  Currency.find({})
    .sort({ creationDate: "ascending" })
    .lean()
    .exec((error, body) => {
      // Currency.find({user: req.user.id}).sort({creationDate:'descending'}).then(currency => {
      res.send(body);
    }); // find something in DB
});

// Todo Index Page
router.get("/", ensureAuthenticated, (req, res) => {
  Currency.find({})
    .sort({ creationDate: "descending" })
    .lean()
    .exec((error, body) => {
      // Currency.find({user: req.user.id}).sort({creationDate:'descending'}).then(currency => {
      res.render("currency/index", {
        currency: body,
      });
    }); // find something in DB
});

// add todo form
router.get("/add", ensureAuthenticated, (req, res) => {
  res.render("currency/add");
});

// edit todo form
router.get("/edit/:id", ensureAuthenticated, (req, res) => {
  Currency.findOne({
    _id: req.params.id,
  })
    .lean()
    .exec((error, body) => {
      // if (currency.user != req.user.id) {
      //   req.flash('error_msg', 'Not authorized');
      //   res.redirect('/currency');
      // } else {
      res.render("currency/edit", {
        currency: body,
      });
      //  };
    });
});

// process  form
router.post("/", ensureAuthenticated, (req, res) => {
  let errors = [];

  if (!req.body.type) {
    errors.push({
      text: "Please add type",
    });
  }
  if (!req.body.unit) {
    errors.push({
      text: "Please add some details",
    });
  }

  if (errors.length > 0) {
    res.render("currency/add", {
      errors: errors,
      type: req.body.type,
      unit: req.body.unit,
      price: req.body.price,
    });
  } else {
    const newUser = {
      type: req.body.type,
      unit: req.body.unit,
      user: req.user.id,
      price: req.body.price,
      title: req.body.title,
      up: req.body.up,
      down: req.body.down,
      change: req.body.change,
    };
    new Currency(newUser).save().then((currency) => {
      req.flash("success_msg", currency.title);
      res.redirect("/currency");
    });
  }
});

// edit form process
router.put("/:id", ensureAuthenticated, (req, res) => {
  Currency.findOne({
    _id: req.params.id,
  }).then((currency) => {
    // new values
    currency.type = req.body.type;
    currency.price = req.body.price;
    currency.unit = req.body.unit;
    currency.up = req.body.up;
    currency.down = req.body.down;
    currency.change = req.body.change;
    currency.title = req.body.title;

    currency.save().then((currency) => {
      req.flash("success_msg", "Currency updated");
      res.redirect("/currency");
    });
  });
});

// delete Todo
router.delete("/:id", ensureAuthenticated, (req, res) => {
  Currency.remove({
    _id: req.params.id,
  }).then(() => {
    req.flash("success_msg", "Currency removed");
    res.redirect("/currency");
  });
});

module.exports = router;
