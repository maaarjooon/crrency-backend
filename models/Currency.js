const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// create schema
const CurrencySchema = new Schema({
  type: {
    type: String,
    required: true,
  },
  unit: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: false,
  },
  title: {
    type: String,
    required: true,
  },
  creationDate: {
    type: Date,
    default: Date.now,
  },
  up: {
    type: String,
    required: false,
  },
  down: {
    type: String,
    required: false,
  },
  change: {
    type: String,
    required: false,
  },
  img: {
    type: String,
    required: false,
  },
});

mongoose.model("currency", CurrencySchema);
